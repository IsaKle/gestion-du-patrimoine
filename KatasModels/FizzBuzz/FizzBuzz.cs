﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KatasExeptions;

namespace KatasModels
{
    public class FizzBuzz
    {
        private int _numero;
        private string _fizzbuzzString;
        
        private FizzBuzz(int numero)
        {
            _numero = numero;
            _fizzbuzzString = FizzBuzzString();
        }
        public static FizzBuzz Generer(int numero)
        {
            if (numero < 15 || numero > 150 )
            {
                throw new FizzBuzzInvalideException();
            }
            return new FizzBuzz(numero);
        }
        
        public String FizzBuzzString()
        {
            String result = "";
            for (int i = 1; i <= _numero; i++)
            {
                if (i%3==0 && i % 5 == 0)
                {
                    result = result + "FizzBuzz";
                }
                else if (i % 3 == 0)
                {
                    result = result + "Fizz";
                }
                else if (i % 5 == 0)
                {
                    result = result + "Buzz";
                }
                else
                {
                    result = result + i.ToString();
                }
            }
            return result;
        }
        public String getString()
        {
            return _fizzbuzzString;
        }

    }
}
