﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasModels.Tennis
{
    public class Joueur
    {
        private int _score;

        public Joueur()
        {
            _score = 0;
        }

        public int getScore()
        {
            return _score;
        }
        public void AddScore()
        {
            switch (_score){
                case 0:
                    _score = 15;
                    break;
                case 15:
                    _score = 30;
                    break;
                default:
                    _score = _score + 10;
                    break;
            }
        }

        public bool ALAvantage(Joueur joueur)
        {
            if (_score > joueur.getScore())
            {
                return true;
            }
            return false;
        }

        
        /*3. Si les deux ont 40, les joueurs sont à égalité.
a.Si le jeu est à égalité, le gagnant d'une balle aura l'avantage et la balle de jeu. 
b.Si le joueur avec l'avantage gagne la balle, il gagne le jeu. 
c.Si le joueur sans avantage gagne, ils sont à égalité.*/

    }
}
