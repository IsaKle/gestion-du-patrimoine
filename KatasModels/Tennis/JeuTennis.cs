﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KatasExeptions.Tennis;

namespace KatasModels.Tennis
{
    public class JeuTennis
    {
        private Joueur _joueur1;
        private Joueur _joueur2;
        private bool _partieTerminer;
        private Joueur _joueurAvantager;

        public JeuTennis(Joueur joueur1, Joueur joueur2) 
        { 
            _joueur1 = joueur1;
            _joueur2 = joueur2;
            _partieTerminer = false;
            _joueurAvantager = joueur1;
        }
        public void Joueur1GagneBalle() 
        {
            _joueur1.AddScore();
            if (_joueurAvantager == _joueur1 
                && _joueur1.getScore()>=40)
                //&& _joueur1.ALAvantage(_joueur2))
            {
                _partieTerminer=true;
            }
            else
            {
                _joueurAvantager = _joueur1;
            }
            
        }

        public void Joueur2GagneBalle() {
            _joueur2.AddScore();
            if (_joueurAvantager == _joueur2
                    && _joueur2.getScore() >= 40)
            //&& _joueur1.ALAvantage(_joueur2))
            {
                _partieTerminer = true;
            }
            else
            {
                
                _joueurAvantager = _joueur2;
            }
        }

        public bool PartieTerminer() { return _partieTerminer; }

        public Joueur Vainqueur()
        {
            if (_partieTerminer)
            {
                return _joueurAvantager;
            }
            else
            {
                throw new VainqueurInvalideException();
            }
        }
            

    }
}
