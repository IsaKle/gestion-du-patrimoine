﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CreditImmo.Exceptions;
using CreditImmo.Modeles;

namespace CreditImmoTest.Modeles
{
    public class DureeTest
    {
        [Fact]
        public void DepassementLimiteInferieure()
        {
            Assert.Throws<DureeInvalideException>(() => new Duree(100));
        }
        [Fact]
        public void DepassementLimiteSuperieure()
        {
            Assert.Throws<DureeInvalideException>(() => new Duree(12*25+1));
        }

        [Fact]
        public void Creation()
        {
            //Assert.True(new Duree(120).get_nombreMois() == 120);
            Assert.Equal((uint)120, new Duree(120).get_nombreMois());
        }

        

    }
}
