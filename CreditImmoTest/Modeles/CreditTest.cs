﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CreditImmo.Exceptions;
using CreditImmo.Modeles;

namespace CreditImmoTest.Modeles
{
    public class CreditTest
    {
        // Pour un capital emprunté de 175.000€, sur une durée de 25 ans à un bon taux, pour un 
        // ingénieur en informatique fumeur atteint de troubles cardiaques
        
        [Fact]
        public void Creation()
        {
            MontantEmprunt me = new MontantEmprunt(175000);
            Duree d = new Duree(25 * 12);
            Risques r = new Risques(false, true, true, true, false);
            EnumTypeTauxNominal ettn = EnumTypeTauxNominal.Bon;
            Credit c = new Credit(me,d,r,ettn);
            Assert.Equal(me, c.get_montantEmprunt());
            Assert.Equal(d, c.get_duree());
            Assert.Equal(r, c.get_risques());
            Assert.Equal(ettn, c.get_enumTypeTauxNominal());
            // fumeur +0.15
            // cardiaque +0.3
            // informaticien -0.05
            // de base +0.3
            // total : 0.7
            Assert.Equal(0.7, c.get_tauxAssurance().get_tauxAssurance(),2);
            // situation : bon taux
            // années : 25 ans
            // résultat du tableau : 1.27 %
            Assert.Equal(1.27, c.get_tauxNominal().get_tauxNominal(),2);
        }

    }
}
