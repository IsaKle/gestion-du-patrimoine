﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CreditImmo.Exceptions;
using CreditImmo.Modeles;

namespace CreditImmoTest.Modeles
{
    public class MontantEmpruntTest
    {
        [Fact]
        public void DepassementLimiteInferieure()
        {
            Assert.Throws<MontantInvalideException>(() => new MontantEmprunt(66));
        }
    }
}
