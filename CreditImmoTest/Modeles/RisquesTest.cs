﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using CreditImmo.Exceptions;
using CreditImmo.Modeles;

namespace CreditImmoTest.Modeles
{
    public class RisquesTest
    {
        

        [Fact]
        public void ModificateurTauxValeurNegative()
        {
            Assert.Equal(-0.10, new Risques(true, false, false, true, false).ModificateurTaux(),2);
        }

        [Fact]
        public void ModificateurTauxValeurPositive()
        {
            Assert.Equal(0.6, new Risques(false, true, true, false, true).ModificateurTaux(),2);
        }

        [Fact]
        public void CreationTrue()
        {
            Risques r = new Risques(true, true, true, true, true);
            Assert.True(r.get_estSportif());
            Assert.True(r.get_estFumeur());
            Assert.True(r.get_estCardiaque());
            Assert.True(r.get_estInformaticien());
            Assert.True(r.get_estPiloteDeChasse());
        }

        [Fact]
        public void CreationFalse()
        {
            Risques r = new Risques(false, false, false, false, false);
            Assert.False(r.get_estSportif());
            Assert.False(r.get_estFumeur());
            Assert.False(r.get_estCardiaque());
            Assert.False(r.get_estInformaticien());
            Assert.False(r.get_estPiloteDeChasse());
        }


    }
}
