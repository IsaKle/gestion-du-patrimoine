﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Exceptions
{
    public class TauxAssuranceInvalideException : Exception
    {
        public TauxAssuranceInvalideException() { }
        public TauxAssuranceInvalideException(string message) : base(message) { }
    }
    
}
