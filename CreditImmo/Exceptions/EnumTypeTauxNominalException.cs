﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Exceptions
{
    public class EnumTypeTauxNominalException : Exception
    {
        public EnumTypeTauxNominalException() { }
        public EnumTypeTauxNominalException(string message) : base(message) { }
        
    }
}
