﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Exceptions
{
    public class TauxNominalInvalideException : Exception
    {
        public TauxNominalInvalideException() { }
        public TauxNominalInvalideException(string message) : base(message) { }
    }
}
