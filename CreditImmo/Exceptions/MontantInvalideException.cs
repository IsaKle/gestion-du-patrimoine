﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Exceptions
{
    public class MontantInvalideException : Exception
    {
        public MontantInvalideException() { }
        public MontantInvalideException(string message) : base(message) { } 
    }
}
