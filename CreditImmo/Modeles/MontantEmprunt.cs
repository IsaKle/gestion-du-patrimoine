﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditImmo.Exceptions;

namespace CreditImmo.Modeles
{
    public class MontantEmprunt
    {
        private static uint _montantEmpruntMinimal = 50000;
        private uint _montantEmprunt;

        public MontantEmprunt(uint montantEmprunt)
        {
            if (montantEmprunt < _montantEmpruntMinimal)
            {
                throw new MontantInvalideException();
            }
            _montantEmprunt = montantEmprunt;
        }
        public static void set_montantEmpruntMinimal(uint montantEmpruntMinimal)
        {
            _montantEmpruntMinimal = montantEmpruntMinimal;
        }
        public uint get_montant()
        {
            return _montantEmprunt;
        }
    }
}
