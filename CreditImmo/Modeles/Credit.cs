﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Modeles
{
    public class Credit
    {
        private MontantEmprunt _montantEmprunt;
        private Duree _duree;
        private Risques _risques;
        private EnumTypeTauxNominal _enumTypeTauxNominal;
        private TauxAssurance _tauxAssurance;
        private TauxNominal _tauxNominal;
       

        public Credit(MontantEmprunt montantEmprunt, Duree duree, Risques risques, EnumTypeTauxNominal enumTypeTauxNominal)
        {
            _montantEmprunt = montantEmprunt;
            _duree = duree;
            _risques = risques;
            _enumTypeTauxNominal = enumTypeTauxNominal;
            _tauxAssurance = TauxAssuranceBuilder.CreerTauxAssurance(risques);
            _tauxNominal = TauxNominalBuilder.CreerTauxNominal(enumTypeTauxNominal, duree);
        }

        
        public float CoutRemboursementMensuel() {
            return _montantEmprunt.get_montant() / _duree.get_nombreMois(); 
        }
        public float CoutTotalInteret() {
            return _montantEmprunt.get_montant() * _tauxNominal.get_tauxNominal();
        }
        public float CoutTotalAssurance() {
            return _montantEmprunt.get_montant() * _tauxAssurance.get_tauxAssurance();
        }
        public float CoutMensuelInteret() {
            return CoutTotalInteret() / _duree.get_nombreMois(); 
        }
        public float CoutMensuelAssurance() { 
            return CoutTotalAssurance() / _duree.get_nombreMois(); 
        }
        public float CalculerMensualite() {
            return CoutRemboursementMensuel() + CoutMensuelAssurance() + CoutMensuelInteret();
        }

        public float MontantRembourseApresMois(uint mois)
        {
            return CalculerMensualite() * mois;
        }
        public float MontantRembourseApresAnnees(uint annees) 
        { 
            return MontantRembourseApresMois(annees*12); 
        }

        public MontantEmprunt get_montantEmprunt()
        {
            return _montantEmprunt;
        }
        public Duree get_duree()
        {
            return _duree;
        }
        public Risques get_risques()
        {
            return _risques;
        }
        public EnumTypeTauxNominal get_enumTypeTauxNominal()
        {
            return _enumTypeTauxNominal;
        }
        public TauxAssurance get_tauxAssurance()
        {
            return _tauxAssurance;
        }
        public TauxNominal get_tauxNominal()
        {
            return _tauxNominal;
        }



        

    }
}
