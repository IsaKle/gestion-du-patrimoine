﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditImmo.Exceptions;

namespace CreditImmo.Modeles
{
    public class TauxAssurance
    {
        private float _tauxAssurance;
        public TauxAssurance(float tauxAssurance)
        {
            if (tauxAssurance < 0 || tauxAssurance > 1)
            {
                throw new TauxAssuranceInvalideException();
            }
            _tauxAssurance = tauxAssurance;
        }
        public float get_tauxAssurance()
        {
            return _tauxAssurance;
        }
    }
}
