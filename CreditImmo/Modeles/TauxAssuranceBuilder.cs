﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditImmo.Exceptions;

namespace CreditImmo.Modeles
{
    public static class TauxAssuranceBuilder
    {
        public static float _tauxAssuranceDeBase = 0.3f;
        public static TauxAssurance CreerTauxAssurance(Risques risque)
        {

            float tauxAssurance = _tauxAssuranceDeBase + risque.ModificateurTaux();
            if (tauxAssurance < 0 || tauxAssurance > 1)
            {
                throw new TauxAssuranceInvalideException();
            }
            return new TauxAssurance(tauxAssurance);
        }
    }
}
