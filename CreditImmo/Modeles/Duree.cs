﻿using CreditImmo.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Modeles
{
    public class Duree
    {
        private static uint _nombreMinMois=9*12; // 9 ans
        private static uint _nombreMaxMois=25*12; //25 ans 
        private uint _nombreMois;

        public Duree(uint nombreMois)
        {   
            if (nombreMois < _nombreMinMois || nombreMois > _nombreMaxMois)
            {
                throw new DureeInvalideException();
            }
            else
            {
                _nombreMois = nombreMois;
            }
        }
        public static void set_nombreMinMois(uint nombreMinMois)
        {
            _nombreMinMois = nombreMinMois;
        }
        public static void set_nombreMaxMois(uint nombreMaxMois)
        {
            _nombreMaxMois = nombreMaxMois;
        }
        public uint get_nombreMois()
        {
            return this._nombreMois;
        }
    }
}
