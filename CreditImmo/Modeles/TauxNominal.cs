﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CreditImmo.Exceptions;

namespace CreditImmo.Modeles
{
    public class TauxNominal
    {
        private float _tauxNominal;
        public TauxNominal(float tauxNominal)
        {
            if (tauxNominal < 0 ) 
            {
                throw new TauxNominalInvalideException();
            }
            _tauxNominal=tauxNominal;
        }
        public float get_tauxNominal()
        {
            return _tauxNominal;
        }

    }
}
