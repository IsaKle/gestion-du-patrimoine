﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Modeles
{
    public class Risques
    {
        private bool _estSportif;
        private bool _estFumeur;
        private bool _estCardiaque;
        private bool _estInformaticien;
        private bool _estPiloteDeChasse;

        public Risques(bool estSportif, bool estFumeur, bool estCardiaque, bool estInformaticien, bool estPiloteDeChasse)
        {
            _estSportif = estSportif;
            _estFumeur = estFumeur;
            _estCardiaque = estCardiaque;
            _estInformaticien = estInformaticien;
            _estPiloteDeChasse = estPiloteDeChasse;
        }


        public float ModificateurTaux()
        {
            float modificateurTaux = 0;
            if (_estSportif)
            {
                modificateurTaux = modificateurTaux - 0.05f;
            }
            if (_estFumeur)
            {
                modificateurTaux = modificateurTaux + 0.15f;
            }
            if (_estCardiaque)
            {
                modificateurTaux = modificateurTaux + 0.3f;
            }
            if (_estInformaticien)
            {
                modificateurTaux = modificateurTaux - 0.05f;
            }
            if (_estPiloteDeChasse)
            {
                modificateurTaux = modificateurTaux + 0.15f;
            }
            return modificateurTaux;
        }



        public bool get_estSportif()
        {
            return _estSportif;
        }
        public bool get_estFumeur()
        {
            return _estFumeur;
        }
        public bool get_estCardiaque()
        {
            return _estCardiaque;
        }
        public bool get_estInformaticien()
        {
            return _estInformaticien;
        }
        public bool get_estPiloteDeChasse()
        {
            return _estPiloteDeChasse;
        }



    }




}
