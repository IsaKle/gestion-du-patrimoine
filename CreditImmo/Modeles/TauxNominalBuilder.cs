﻿using CreditImmo.Exceptions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreditImmo.Modeles
{
    public static class TauxNominalBuilder
    {
        private static List<float> tableauDeTauxNominalBon = new List<float>() { 0.62f, 0.67f, 0.85f, 1.04f, 1.27f };
        private static List<float> tableauDeTauxNominalTresBon = new List<float>() { 0.43f, 0.55f, 0.73f, 0.91f, 1.15f };
        private static List<float> tableauDeTauxNominalExcellent = new List<float>() { 0.35f, 0.45f, 0.58f, 0.73f, 0.89f };
        public static TauxNominal CreerTauxNominal(EnumTypeTauxNominal typeTauxNominal, Duree duree)
        {
            List<float> tableauDeTauxNominal;
            float tauxNominal;
            uint dureeEnMois = duree.get_nombreMois();
            switch (typeTauxNominal)
            {
                case EnumTypeTauxNominal.Bon:
                    tableauDeTauxNominal = tableauDeTauxNominalBon;
                    break;
                case EnumTypeTauxNominal.TresBon:
                    tableauDeTauxNominal = tableauDeTauxNominalTresBon;
                    break;
                case EnumTypeTauxNominal.Excellent:
                    tableauDeTauxNominal = tableauDeTauxNominalExcellent;
                    break;
                default:
                    throw new EnumTypeTauxNominalException();
            }
            switch (dureeEnMois)
            {
                case > 288: // 25 ans et +
                    tauxNominal = tableauDeTauxNominal[4];
                    break;
                case > 240: // 20 ans et +
                    tauxNominal = tableauDeTauxNominal[3];
                    break;
                case > 180: // 15 ans et +
                    tauxNominal = tableauDeTauxNominal[2];
                    break;
                case > 120: // 10 ans et +
                    tauxNominal = tableauDeTauxNominal[1];
                    break;
                case > 84: // 7 ans et +
                    tauxNominal = tableauDeTauxNominal[0];
                    break;
                default:
                    throw new DureeInvalideException();
            }

            return new TauxNominal(tauxNominal);
        }
    }
}
