﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using KatasModels;
using KatasExeptions;

namespace KatasTests
{
    public class FizzBuzzTest
    {
        [Fact]
        public void DepassementLimiteInferieure()
        {
            Assert.Throws<FizzBuzzInvalideException>(() => FizzBuzz.Generer(10));
        }
        [Fact]
        public void DepassementLimiteSuperieure()
        {
            Assert.Throws<FizzBuzzInvalideException>(() => FizzBuzz.Generer(200));
        }
        [Fact]
        public void FizzBuzz15()
        {
            Assert.Equal("12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz",FizzBuzz.Generer(15).getString());
        }
        
        
    }
}
