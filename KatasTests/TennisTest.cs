﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using KatasModels.Tennis;
using KatasExeptions.Tennis;


namespace KatasTests
{
    public class TennisTest
    {
        [Fact]
        public void TennisScoreNouveauJoueurNul()
        {
            Assert.Equal(0, new Joueur().getScore());
        }
        [Fact]
        public void TennisAjoutScoreJoueur()
        {
            Joueur joueur = new Joueur();
            joueur.AddScore();
            Assert.Equal(15, joueur.getScore());
            joueur.AddScore();
            Assert.Equal(30, joueur.getScore());
            joueur.AddScore();
            Assert.Equal(40, joueur.getScore());
            joueur.AddScore();
            Assert.Equal(50, joueur.getScore());
        }
        [Fact]
        public void TennisJoueurALAvantage()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            joueur1.AddScore();
            Assert.True(joueur1.ALAvantage(joueur2));
        }

        [Fact]
        public void Joueur1NeGagnePasAprès1Manche()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 15
            Assert.False(jeuTennis.PartieTerminer());
            
        }

        [Fact]
        public void Joueur1GagneAprès3Manche()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 15
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 30
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 40
            Assert.True(jeuTennis.PartieTerminer());
        }

        [Fact]
        public void PasDeVainqueurEnDebutDePartie()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            Assert.Throws<VainqueurInvalideException>(() => jeuTennis.Vainqueur());
            
        }

        [Fact]
        public void Joueur1EstVainqueurAprès3Manche()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 15
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 30
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 40
            Assert.Equal(joueur1, jeuTennis.Vainqueur());
        }

        [Fact]
        public void JeuEgalite()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 15
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 15
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 30
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 30
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 40
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 40
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 50
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 50
            Assert.False(jeuTennis.PartieTerminer());
            Assert.Throws<VainqueurInvalideException>(() => jeuTennis.Vainqueur());
        }


        [Fact]
        public void Joueur2GagneApres4Manches()
        {
            Joueur joueur1 = new Joueur();
            Joueur joueur2 = new Joueur();
            JeuTennis jeuTennis = new JeuTennis(joueur1, joueur2);
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 15
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 15
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 30
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 30
            jeuTennis.Joueur1GagneBalle(); // score joueur1 = 40
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 40
            jeuTennis.Joueur2GagneBalle(); // score joueur2 = 50
            Assert.True(jeuTennis.PartieTerminer());
            Assert.Equal(joueur2, jeuTennis.Vainqueur());
        }

    }
}
