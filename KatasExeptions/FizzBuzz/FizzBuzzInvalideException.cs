﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasExeptions
{
    public class FizzBuzzInvalideException : Exception
    {
        public FizzBuzzInvalideException() { }
        public FizzBuzzInvalideException(string message) : base(message) { }
    }
}
