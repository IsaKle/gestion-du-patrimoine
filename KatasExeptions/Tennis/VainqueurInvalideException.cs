﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KatasExeptions.Tennis
{
    public class VainqueurInvalideException : Exception
    {
        public VainqueurInvalideException() { }
        public VainqueurInvalideException(string message) : base(message) { }
    }
}
